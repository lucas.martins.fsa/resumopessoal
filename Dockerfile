FROM nginx:1.19.4-alpine
LABEL maintainer="Lucas Martins Arruda <lucas.martins.fsa@gmail.com>"
COPY . /usr/share/nginx/html
EXPOSE 80
